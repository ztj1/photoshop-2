# Image Processor

An image processing library created by Zach Johnson and Alex Bjerke for COM S 311. Capable of making stitch and width cuts for changing the dimensions of images while also causing minimal distortion. Cuts are made based on calculated pixel importances. Pixels with high importance are less likely to be removed by the algorithm.

The intention of the assignment was to evaluate our understanding of and ability to implement iterative dynamic programming algorithms.