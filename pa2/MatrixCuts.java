package pa2;

import java.util.ArrayList;
import java.util.Stack;

public class MatrixCuts {


    public static ArrayList<Tuple> widthCut(int[][] M) {
        //Find the number of rows and columns
        int rows = M.length;
        int cols = M[0].length;

        //create a cost array
        int[][] costArr = new int[rows][cols];

        ArrayList<Tuple> resultList = new ArrayList<>();

        if(rows <= 0){
            return resultList;
        }

        //initalize the first row in the cost array with M's first row
//        for(int k = 0; k < cols; k++){
//            costArr[0][k] = M[0][k];
//        }
        System.arraycopy(M[0], 0, costArr[0], 0, costArr[0].length);

        //initialize all values in cost array to -1 (untouched)
        for(int x = 1; x < rows; x++){
            for(int y = 0; y < cols; y++){
                costArr[x][y] = -1;
            }
        }


        //Iterate through M to dynamically update costArr
        for(int i = 0; i < rows - 1; i++){
            for(int j = 0; j < cols; j++){
                int curCost = costArr[i][j];
                int costDown1 = costArr[i + 1][j];

                //check and update straight down one (same column, next row) ALWAYS
                if((costDown1 < 0) || ((curCost + M[i + 1][j]) < costDown1)) {
                    costArr[i + 1][j] = curCost + M[i + 1][j];
                }
                if(j == 0 && (j != (cols - 1))) {
                    //We are at the first column. Next row, one column right ONLY
                    int costDownRight = costArr[i + 1][j + 1];
                    if((costDownRight < 0) || ((curCost + M[i + 1][j + 1]) < costDownRight)){
                        costArr[i + 1][j + 1] = curCost + M[i + 1][j + 1];
                    }
                }
                else if(j == (cols - 1)){
                    //We are at the last column. Next row, one column left ONLY
                    int costDownLeft = costArr[i + 1][j - 1];
                    if((costDownLeft < 0) || ((curCost + M[i + 1][j - 1]) < costDownLeft)){
                        costArr[i + 1][j - 1] = curCost + M[i + 1][j - 1];
                    }
                }
                else{
                    //Next row, one column left
                    int costDownLeft = costArr[i + 1][j - 1];
                    if((costDownLeft < 0) || ((curCost + M[i + 1][j - 1]) < costDownLeft)){
                        costArr[i + 1][j - 1] = curCost + M[i + 1][j - 1];
                    }
                    //Next row, one column right
                    int costDownRight = costArr[i + 1][j + 1];
                    if((costDownRight < 0) || ((curCost + M[i + 1][j + 1]) < costDownRight)){
                        costArr[i + 1][j + 1] = curCost + M[i + 1][j + 1];
                    }
                }
            }
        }
        //Cost array is now completely filled out.
        //Find the LEFTMOST, smallest cost in the bottom row
        int leastCostColIndex = cols - 1;
        int leastCostFound = costArr[rows - 1][cols - 1];
        for(int z = cols - 2; z >= 0; z--){

            /////////////////////POSSIBLE ARRAY INDEX OUT BOUNDS/////////////////
            if(costArr[rows - 1][z] <= leastCostFound){
                leastCostColIndex = z;
                leastCostFound = costArr[rows - 1][z];
            }
        }

        //Get ready to backtrack by starting at the cell of the least total cost in bottom row
        Tuple endPoint = new Tuple(rows - 1,leastCostColIndex);
        Stack<Tuple> backtrackStack = new Stack<>();
        backtrackStack.push(endPoint);
        int btColIndex = leastCostColIndex;

        //Now backtrack
        int up1 = -1;
        int upLeft = -1;
        int upRight = -1;
        for(int bt = rows - 1; bt > 0; bt--){                   ////ARRAY INDEX OOB
            up1 = costArr[bt - 1][btColIndex];
            if(btColIndex != 0 || btColIndex < cols - 1) { //  //  // //
                if (btColIndex == 0) {
                    upLeft = -1;
                    upRight = costArr[bt - 1][btColIndex + 1];
                } else if (btColIndex == cols - 1) {
                    upLeft = costArr[bt - 1][btColIndex - 1];
                    upRight = -1;
                } else {
                    upLeft = costArr[bt - 1][btColIndex - 1];
                    upRight = costArr[bt - 1][btColIndex + 1];
                }
            }
            else{
                upLeft = -1;
                upRight = -1;
            }
            if((upLeft != -1) && (upLeft <= up1)){
                if((upRight == -1) || (upLeft < upRight)) {
                    btColIndex -= 1;
                }
            }
            else if((upRight != -1) && (upRight < up1)){
                btColIndex += 1;
            }

            backtrackStack.push(new Tuple(bt-1, btColIndex));
            }

        ArrayList<Tuple> cut = new ArrayList<>();

        cut.add(new Tuple(leastCostFound, -1));

        while (!backtrackStack.isEmpty()) {
            cut.add(backtrackStack.pop());
        }
        return cut;
    }

    public static ArrayList<Tuple> stitchCut(int[][] M) {
        //TODO
        //Find the number of rows and columns
        int rows = M.length;
        int cols = M[0].length;

        //create a cost array
        int[][] costArr = new int[rows][cols];

        ArrayList<Tuple> resultList = new ArrayList<>();

        if(rows <= 0){
            return resultList;
        }

        //initalize the first row in the cost array with M's first row
//        for(int k = 0; k < cols; k++){
//            costArr[0][k] = M[0][k];
//        }
        System.arraycopy(M[0], 0, costArr[0], 0, costArr[0].length);

        //initialize all values in cost array to -1 (untouched)
        for(int x = 1; x < rows; x++){
            for(int y = 0; y < cols; y++){
                costArr[x][y] = -1;
            }
        }

        //Iterate through M to dynamically update costArr
        for(int i = 0; i < rows - 1; i++){
            for(int j = 0; j < cols; j++){
                int curCost = costArr[i][j];
                int costDown1 = costArr[i + 1][j];

                //check and update straight down one (same column, next row) ALWAYS
                if((costDown1 < 0) || ((curCost + M[i + 1][j]) < costDown1)) {
                    costArr[i + 1][j] = curCost + M[i + 1][j];
                }
                if(j != (cols - 1)){
                    //We are at the last column. Same row, one column right
                    int costRight = costArr[i][j + 1];
                    if((costRight < 0) || ((curCost + M[i][j + 1]) < costRight)){
                        costArr[i][j + 1] = curCost + M[i][j + 1];
                    }
                    //We are at the first column. Next row, one column right
                    int costDownRight = costArr[i + 1][j + 1];
                    if((costDownRight < 0) || ((curCost + M[i + 1][j + 1]) < costDownRight)){
                        costArr[i + 1][j + 1] = curCost + M[i + 1][j + 1];
                    }
                }
            }
        }
        //Cost array is now completely filled out.
        //Find the LEFTMOST, smallest cost in the bottom row
        int leastCostColIndex = cols - 1;
        int leastCostFound = costArr[rows - 1][cols - 1];
        for(int z = cols - 2; z >= 0; z--){
            if(costArr[rows - 1][z] <= leastCostFound){
                leastCostColIndex = z;
                leastCostFound = costArr[rows - 1][z];
            }
        }

        //Get ready to backtrack by starting at the cell of the least total cost in bottom row
        Tuple endPoint = new Tuple(rows - 1,leastCostColIndex);
        Stack<Tuple> backtrackStack = new Stack<>();
        backtrackStack.push(endPoint);
        int btColIndex = leastCostColIndex;

        //Now backtrack
        int up1;
        int upLeft = -1;
        int left1 = -1;
        int btRowIndex = rows - 1;
        while(btRowIndex > 0){
            up1 = costArr[btRowIndex - 1][btColIndex];  ////ARRAY INDEX OOB
            if(btColIndex != 0) {
                left1 = costArr[btRowIndex][btColIndex - 1];
                upLeft = costArr[btRowIndex - 1][btColIndex - 1];
                if (left1 <= upLeft && left1 <= up1) {
                    btColIndex -= 1;
                }
                else if (upLeft <= up1) {
                    btColIndex -= 1;
                    btRowIndex -= 1;
                }
            }
            else{
                btRowIndex -= 1;
            }
            backtrackStack.push(new Tuple(btRowIndex, btColIndex));
        }

        ArrayList<Tuple> cut = new ArrayList<>();

        cut.add(new Tuple(leastCostFound, -1));

        while (!backtrackStack.isEmpty()) {
            cut.add(backtrackStack.pop());
        }
        return cut;
    }
}