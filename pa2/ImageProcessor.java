package pa2;


import java.awt.*;
import java.util.ArrayList;



public class ImageProcessor {

    public static Picture reduceWidth(int x, String inputImage) {
        Picture before = new Picture(inputImage);

        for (int i = 0; i < x; i++) {
            int[][] importance = importanceMatrix(before);
            ArrayList<Tuple> cut = MatrixCuts.widthCut(importance);

            if (cut != null) {
                before = removeCut(before, cut);
            }
            else {
                System.out.println("BAD BAD CUT METHOD BAD");
            }
        }
        return before;
    }

    private static Picture removeCut(Picture before, ArrayList<Tuple> cut) {

        Picture temp = new Picture(before.width()-1, before.height());

        int i = 0;

        for (int t = 1; t < cut.size(); t++) {
            int k = 0;
            for (int j = 0; j < before.width(); j++) {
                if (j != cut.get(t).getY()) {
                    temp.set(k, i, before.get(j,i));
                    k++;
                }
            }
            i++;
        }
        return temp;
    }

    private static int[][] importanceMatrix(Picture pic) {

        int[][] impArr = new int[pic.height()][pic.width()];

        for (int i = 0; i < impArr.length; i++) {
            for (int j = 0; j < impArr[0].length; j++) {
                int imp;
                if (j > 0 && j < impArr[0].length-1) {
                    imp = importance(pic.get(j-1, i), pic.get(j+1, i));
                }
                else if (j == 0) {
                    imp = importance(pic.get(j, i), pic.get(j+1, i));
                }
                else {
                    imp = importance(pic.get(j-1, i), pic.get(j, i));
                }
                impArr[i][j] = imp;
            }
        }
        return impArr;
    }

    private static int importance(Color one, Color two) {
        int r1 = one.getRed();
        int g1 = one.getGreen();
        int b1 = one.getBlue();
        int r2 = two.getRed();
        int g2 = two.getGreen();
        int b2 = two.getBlue();
        int r = (r1-r2)*(r1-r2);
        int g = (g1-g2)*(g1-g2);
        int b = (b1-b2)*(b1-b2);

        return (r+g+b);
    }
}